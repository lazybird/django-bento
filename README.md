
Django Bento
============

Django Bento is hosted [here][github].

     __________________
    /\  ______________ \
    ::\ \ZZZZZZZZZZZZ/\ \
     \.\ \  ________ \:\ \     django-bento helps you adding editable
      \:\ \ \   \   \ \:\ \    text and image content areas on
       \:\ \ \   \   \ \:\ \   your site. These content boxes are
        \:\ \ \___\___\ \:\ \  easy to define in templates and are
         \:\ \ \   \   \ \:\ \ updated from Django admin.
          \:\ \ \   \   \ \:\ \
           \:\ \ \___\___\ \;\ \
            \:\_________________\
             :/ZZZZZZZZZZZZZZZZZ/




[github]: https://github.com/lazybird/django-bento
